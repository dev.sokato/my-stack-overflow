import multiprocessing as mp
import logging
from functools import partial
import numpy as np
import sklearn.metrics.pairwise as sklearn
import pdb
# mpl = mp.log_to_stderr()
# mpl.setLevel(logging.INFO)
gd1 = 586 # 585 is the limit for hanging
gd2 = 14

def bar(y, x):
    mul = x @ y.T
    # mul = 1
    return mul

def foo(p):
    mat1 = np.ones((gd1,gd2))
    test = (np.ones((1,gd2)), np.ones((1,gd2)))
    #Pooldebugin
    result = p.map(partial(bar, x=mat1), test,chunksize)
    # print(result)

if __name__ == "__main__":
    sklearn.rbf_kernel(np.ones((gd1, gd2)), np.ones((gd1, gd2)))
    print("SKLEARN_COMPLETED")
    # pdb.set_trace()
    p = mp.Pool(2)
    foo(p)
    p.close()
    # p.join()
    print('FOO_COMPLETED')
