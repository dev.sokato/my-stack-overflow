variable=3.14185
print(f'This is a variable: {variable}')
print("Number {:03d} is here.".format(11))
print('A formatted number - {:.4f}'.format(.2))
data = [
    {'a':10, 'b': 0, 'c': 2},
    {'a':7, 'b': 4, 'c': 4},
    {'a':4, 'b': 5, 'c': 3}
]
