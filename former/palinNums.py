
import time

def palSum(N):
    multiple = 9
    #Find lower limit
    minBound = multiple*((10**(N-1))//multiple)
    minBound += multiple if len(str(minBound))< N else 0
    #Find upper limit
    maxBound = multiple*((10**(N))//multiple)
    maxBound += multiple if len(str(maxBound))<N+1 else 0
    print(minBound,maxBound)
    #Get sum
    sum = 0
    for num in range(int(minBound),int(maxBound),multiple):
        nstr = str(num)
        if '0' not in nstr and nstr[::-1]==nstr:
            sum+=num
    return sum


def getSum(N):
    sum=0
    first=pow(10,N-1)
    last=pow(10,N)
    for i in range(first,last):
        if(i%9==0):
            if(palindrome(i)):
                sum+=i
    return sum%(pow(10,9)+7)

def palindrome(num):
    rev=0
    n=num

    while(n>0):
        rem=n%10
        if(rem==0):
            return False
        rev=(rev*10)+rem
        n=n//10

    if(num==rev):
        return True
    else:
        return False

if __name__ == "__main__":
    N = 10
    s1 = time.time()
    print(getSum(N))
    s2 = time.time()
    print(palSum(N))
    s3 = time.time()
    print(s2-s1,s3-s2)
