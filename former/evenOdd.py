def exactlyOneOdd(a,b):
    return (a+b)%2!=0

print(exactlyOneOdd(3,4))
print(exactlyOneOdd(4,4))
print(exactlyOneOdd(3,3))
